#!venv/bin/python
import sys
import os
import numpy as np
from math import pi, sin, cos
import scipy.signal as signal
from matplotlib import pyplot as plt
import itertools

def plot_freq_response(h, label=None):
    w,H = signal.freqz(h)
    plt.title("Frequency Response")
    plt.plot(w/2/pi, 20*np.log10(abs(H)), label=label)
    #plt.plot(w/np.pi, abs(h),'b')  # Linear
    plt.ylabel('Amplitude (dB)')
    plt.xlabel(r'Normalized Frequency ($\pi$ rad/sample)')
    # angles = np.unwrap(np.angle(h))
    # plt.plot(w/2/pi, angles, 'g')  # Phase Response
    # plt.ylabel('Angle (radians)', color = 'g')


def plot_samples(h):
    # plt.stem(h, use_line_collection=True)
    plt.stem(h)

def upsample_filter(h):
    # Upsample by four by inserting three zeros after every sample
    return np.array(list(itertools.chain.from_iterable([[x, 0, 0, 0] for x in h])))

def downsample_filter(h, offset=0):
    # downsample by four by decimating
    return h[offset::4] * 4



def write_to_verilog_file(filename, text):
    """Markers defining start and end of text are //START and //END"""
    orig_file = open(filename).read()
    if text[-1] == '\n': text = text[:-1]
    assert len(orig_file.split("//START")) == 2 and len(orig_file.split("//END")) == 2
    new_file = orig_file.split("//START")[0] + "//START\n" + text + "\n//END" + orig_file.split("//END")[1]
    with open(filename, 'w') as f:
        f.write(new_file)

def get_worst_case_values(h_sd):
    return [2 ** 17 - 1 if x > 0 else -2 ** 17 for x in h_sd]

def plot_stopband_keepout(ref_filter=None):
    if ref_filter is None:
        ref = 0
    else:
        w, H = signal.freqz(ref_filter)
        ref = 20 * np.log10(max(abs(H)))
    plt.plot([0.14, 0.14], [0, -40])
    plt.plot([0.1752, 0.1752], [0, -40])
    plt.plot([0.42, 0.42], [0, -40])


from timeit import default_timer as timer

points = 4000
signal_edge = int(0.14 * 2 * points)
ob1_edge = int(0.1752 * 2 * points)
ob2_edge = int(0.42 * 2 * points)
ob3_edge = int(0.3 * 2 * points)


def get_band_powers(h):
    """Returns power for signal, OB1, OB2, OB3"""

    # ts = timer()
    w, H = signal.freqz(h, worN=points)
    # print((timer() - ts) * 1000000)

    # 85 for 2048
    # 125 us for 4096
    # 1200 us for 32768

    H = np.abs(H)

    H_signal = H[0:signal_edge] * 2
    H_OB1 = H[signal_edge:ob1_edge]
    H_OB2 = H[ob1_edge:ob2_edge]
    H_OB3 = np.concatenate((H[ob2_edge:], H[ob3_edge:]))
    # H_signal = [H for w, H in zip(w, H) if w/2/pi < 0.14] * 2
    # H_OB1 = [H for w, H in zip(w, H) if 0.14 < w/2/pi < 0.1752]
    # H_OB2 = [H for w, H in zip(w, H) if 0.1752 < w/2/pi < 0.42]
    # H_OB3 = [H for w, H in zip(w, H) if 0.42 < w/2/pi] + [abs(H) for w, H in zip(w, H) if 0.3 < w/2/pi]  # 0.42-0.7 == 0.42-0.5 + 0.3-0.5
    return 20*np.log10(np.sum(H_signal) / points), 20*np.log10(np.sum(H_OB1) / points), 20*np.log10(np.sum(H_OB2) / points), 20*np.log10(np.sum(H_OB3) / points)


def get_attenuation_errors(h):
    """Returns error values in db for OB1, OB2, OB3. positive is good, negitive is bad"""
    sig, ob1, ob2, ob3 = get_band_powers(h)
    # print(get_band_powers(h))
    # print(sig - ob1, sig - ob2, sig - ob3)
    return sig - ob1 - 58, sig - ob2 - 60, sig - ob3 - 63


def get_stopband_error(h):
    w, H = signal.freqz(h)
    H_stopband = [abs(H) for w, H in zip(w, H) if w/2/pi > 0.2]
    H_max = max(abs(H))
    # H_max = H_max * 0.0 + 1
    H_stopband_max = 20*np.log10(max(H_stopband/H_max))
    return H_stopband_max + 40 if H_stopband_max > -40 else 0

def measure_mer(h_tx, h_rx, truncate=True):
    h_tx_sd = h_tx * 2 ** 17
    h_rx_sd = h_rx * 2 ** 17
    if truncate and False:
        h_tx_sd = np.rint(h_tx_sd)
        h_rx_sd = np.rint(h_rx_sd)
    h = np.convolve(h_tx_sd, h_rx_sd)
    middle = int((len(h)-1) / 2)
    error_terms = [i for i in range(middle % 4, len(h)-1, 4) if i != middle]
    P_sig = h[middle] ** 2
    P_isi = sum([h[i] ** 2 for i in error_terms])
    return 10 * np.log10(P_sig/P_isi)
    

def get_sinc_samples(N, fc):
    n = np.arange(0, N)
    return 2*fc*np.sinc(2*fc*(n-(N-1)/2))


def get_stats(h, h_recv):
    ob1, ob2, ob3 = get_attenuation_errors(h)
    return measure_mer(h, h_recv), ob1, ob2, ob3


def print_stats(h, h_recv, name = ""):
    mer, ob1, ob2, ob3 = get_stats(h, h_recv)
    print(f"{name} MER: {mer:0.3f}, OB1: {ob1:0.3f}, OB2: {ob2:0.3f}, OB3: {ob3:0.3f}")


def scale_by_worst_case_input(h):
    # print("Dividing by ", sum(abs(h)))
    return h / sum(abs(h))
