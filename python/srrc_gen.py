#!venv/bin/python
import numpy as np
from math import pi, sin, cos


def get_srrc_samples(N, beta):
    N_sps = 4  # number of samples per symbol

    h = np.zeros(N)
    for i, n in enumerate(np.linspace(-(N-1)/2, (N-1)/2, N)):
        if n == 0:
            h[i] = 1/N_sps + beta / N_sps * (4/pi - 1)
        elif abs(abs(n) - abs(N_sps / (4 * beta))) < 0.00001:
            A = cos(pi / (4 * beta) * (1 + beta))
            B = cos(pi / (4 * beta) * (1 - beta))
            h[i] = -beta / N_sps * ((2 / pi * A) - B)
        else:
            A = cos(pi * (1 + beta) * n / N_sps)
            B = sin(pi * (1 - beta) * n / N_sps)
            numerator = (4 * beta * n / N_sps * A) + B
            denominator = (1 - (4 * beta * n / N_sps) ** 2) * pi * n
            h[i] = numerator / denominator
    return h

