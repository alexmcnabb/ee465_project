#!venv/bin/python
import sys
import os
import numpy as np
from math import pi, sin, cos
import scipy.signal as signal
from matplotlib import pyplot as plt

from srrc_gen import get_srrc_samples
import filt_gen

from tools import plot_samples, plot_freq_response
import tools


# def gen_recv_filter():
#     h = get_srrc_samples(beta=0.25)
#     # Scale for worst case input
#     scale_factor = sum(abs(h))
#     h = h / scale_factor
#     return h


# def gen_tx_filter():
#     # h = get_srrc_samples(beta=0.4325)
#     # h = h * signal.windows.kaiser(21, beta=1.66)
#     # Use fancy filter
#     h = np.array([0.00091016351328832, 0.006191061321883938, 0.013438214506682613, -3.062876856045641e-05, -0.021434132463639573, -0.041798129272845444, -0.027862475292505098, 0.0385700192961055, 0.14456522496509283, 0.24456531512601812, 0.28577073413695847, 0.24456531512601812, 0.14456522496509283, 0.0385700192961055, -0.027862475292505098, -0.041798129272845444, -0.021434132463639573, -3.062876856045641e-05, 0.013438214506682613, 0.006191061321883938, 0.00091016351328832])
#     # Scale for worst case input
#     scale_factor = sum(abs(h))
#     h = h / scale_factor
#     return h


# def export_recv_filter():
#     h = gen_recv_filter()
#     h_sd = np.rint(h * 2 ** 17)
#     text = ""
#     for i, val in enumerate(h_sd):
#         text = text + f"assign b[{i}] = {'-' if val < 0 else ''}18'sd{abs(int(val))};\n"
#     tools.write_to_verilog_file("../filter_recv.v", text)
#     worst_case_values = tools.get_worst_case_values(h_sd)
#     with open('../functional_simulation/recv_filter_worst_case_input.txt', 'w') as f:
#         f.write("\n".join([str(x) for x in worst_case_values]))


# def export_tx_filter():
#     h = gen_tx_filter()
#     h_sd = np.rint(h * 2 ** 17)
#     text = ""
#     for i, val in enumerate(h_sd):
#         text = text + f"assign b[{i}] = {'-' if val < 0 else ''}18'sd{abs(int(val))};\n"
#     tools.write_to_verilog_file("../filter_tx_raw.v", text)
#     worst_case_values = tools.get_worst_case_values(h_sd)
#     with open('../functional_simulation/tx_filter_worst_case_input.txt', 'w') as f:
#         f.write("\n".join([str(x) for x in worst_case_values]))


# def export_reference_impul_resp():
#     h_recv = gen_recv_filter()
#     h_recv = np.rint(h_recv * 2 ** 17)
#     h_tx = gen_tx_filter()
#     h_tx = np.rint(h_tx * 2 ** 17)
#     h = np.convolve(h_tx, h_recv)
#     h = np.rint(h / 2 ** 17)
#     h = [0] + h.tolist()  # Account for total delay of system
#     with open('../functional_simulation/correct_tx_rx_impulse_response.txt', 'w') as f:
#         f.write("\n".join([str(int(x)) for x in h]))


# def export_multless_tx_filter():
#     h = gen_tx_filter()
#     h_sd = np.rint(h * 2 ** 17)
#     lut_size = 36  # Make larger then 18 to improve truncation in adders. Must match verilog parameter
#     # a = np.array([-1, -1/3, 1/3, 1, 0]) * (2 ** (lut_size - 19))
#     a = np.array([-0.75, -0.25, 0.25, 0.75, 0]) * (2 ** (lut_size - 19))
#     lines = []
#     for i, b in enumerate(h_sd):
#         lines.append(f"    casex (x[{i}])")
#         for bits, num in zip(['100', '101', '111', '110', '0XX'], b * a):
#             sign = "-" if num < 0 else ""
#             lines.append(f"        3'b{bits}: mult_out[{i}] <= {sign}{lut_size}'sd{int(abs(num))};")
#         lines.append(f"    endcase")
#     tools.write_to_verilog_file("../filter_tx_multless.v", "\n".join(lines))
# for b in np.arange(0.09, 0.15, 0.01):
Ns = range(50, 150)
mers = []
for N in Ns:
    h = get_srrc_samples(N, beta=0.12)
    mer = tools.measure_mer(h, h)
    mers.append(mer)

plt.plot(Ns, mers)
# plt.legend()
plt.title("MER vs length of gold standard TX and RX filters")
plt.xlabel("N")
plt.ylabel("MER")
plt.show()

exit()




# Efficient rx filter lengths for multiplier sharing - 9 -> 71, 72     10 -> 79, 80
use_evens = True

"""RX Gold filter"""
h_rx = get_srrc_samples(N=72 if use_evens else 71, beta=0.12)
h_rx = tools.scale_by_worst_case_input(h_rx)
filt_gen.build_filter_withmult("../filter_rx_gold.v", h=h_rx)


"""TX Gold filter"""
h_tx = get_srrc_samples(N=72 if use_evens else 71, beta=0.12)
filt_gen.build_filter_multless("../filter_tx_gold.v", h=h_tx)


"""TX Practical filter"""
# Best possible with unmodified srrc
# h_prac = get_srrc_samples(N=110 if even else 1, beta=0.12)
# Best possible with srrc + kaiser window
# h_prac = get_srrc_samples(N=76 if use_evens else 77, beta=0.11)
# h_prac = h_prac * signal.kaiser(len(h_prac), beta=1.2)
# Best possible with fancy filter generation
# N=72
h_prac = [7.133877031517092e-05, 0.0013225794534972768, 0.002447723055139963, 0.0028108828237708725, 0.0009495412429366196, -0.001645743215692688, -0.003948270353117022, -0.003821068731934755, -0.001276105868773184, 0.0026589857225610978, 0.005695626690602838, 0.005469781329913707, 0.0016458708175120876, -0.004353387610173874, -0.008603187522341084, -0.00792982896665759, -0.001496707352940264, 0.00747419385241843, 0.01310375280832862, 0.010814654638137532, 0.00029980387502287676, -0.0126406025610543, -0.019566663534622337, -0.014275924170197138, 0.002348917251604787, 0.02128922373659184, 0.030159269872391103, 0.019863907451268314, -0.007920707291525268, -0.03956322928944739, -0.054224673452689165, -0.03371297431053666, 0.026529410318219548, 0.11336220640608165, 0.19855564755521263, 0.251312108518588, 0.251312108518588, 0.19855564755521263, 0.11336220640608165, 0.026529410318219548, -0.03371297431053666, -0.054224673452689165, -0.03956322928944739, -0.007920707291525268, 0.019863907451268314, 0.030159269872391103, 0.02128922373659184, 0.002348917251604787, -0.014275924170197138, -0.019566663534622337, -0.0126406025610543, 0.00029980387502287676, 0.010814654638137532, 0.01310375280832862, 0.00747419385241843, -0.001496707352940264, -0.00792982896665759, -0.008603187522341084, -0.004353387610173874, 0.0016458708175120876, 0.005469781329913707, 0.005695626690602838, 0.0026589857225610978, -0.001276105868773184, -0.003821068731934755, -0.003948270353117022, -0.001645743215692688, 0.0009495412429366196, 0.0028108828237708725, 0.002447723055139963, 0.0013225794534972768, 7.133877031517092e-05]

# h_prac = [0, 0.9, 0.9, 0.9, 0.9, 0]
# h_prac = get_srrc_samples(N=72, beta=0.12)

h_prac = np.array(h_prac)
filt_gen.build_filter_multless("../filter_tx_practical.v", h=h_prac)


# Up and Down sampler filter
# Filters designed in Matlab filter designer, Fpass = 0.875, Fstop = 5.8125, magnitude weighting Wpass:Wstop
# N=16, 100:1 (Good) !!!!!!!!!!!!!!!
h_updown = [4.178693e-04, 4.254918e-03, 5.510671e-03, -3.507739e-03, -2.370306e-02, -3.558937e-02, -6.420197e-03, 8.016023e-02, 1.970396e-01, 2.818441e-01, 2.818441e-01, 1.970396e-01, 8.016023e-02, -6.420197e-03, -3.558937e-02, -2.370306e-02, -3.507739e-03, 5.510671e-03, 4.254918e-03, 4.178693e-04]
# N=16, 1:1 (Good)
# h_updown = [-1.820679e-03, -9.778165e-03, -1.959365e-02, -1.690484e-02, 1.883427e-02, 9.390040e-02, 1.855416e-01, 2.493743e-01, 2.493743e-01, 1.855416e-01, 9.390040e-02, 1.883427e-02, -1.690484e-02, -1.959365e-02, -9.778165e-03, -1.820679e-03]
# N=16, 1:10 (Prac passes, gold fails)
# h_updown = [-2.910861e-03, -1.036836e-02, -1.799092e-02, -1.154050e-02, 2.588829e-02, 9.761994e-02, 1.818552e-01, 2.394725e-01, 2.394725e-01, 1.818552e-01, 9.761994e-02, 2.588829e-02, -1.154050e-02, -1.799092e-02, -1.036836e-02, -2.910861e-03]
# N=16, 1:100 (Fails)
# h_updown = [-1.729508e-03, -5.602567e-03, -7.456395e-03, 3.576338e-03, 3.927039e-02, 1.004682e-01, 1.692604e-01, 2.154204e-01, 2.154204e-01, 1.692604e-01, 1.004682e-01, 3.927039e-02, 3.576338e-03, -7.456395e-03, -5.602567e-03, -1.729508e-03]



# Fpass=0.875, Fstop=5.375, 1:1
# h_updown = [-4.420744e-03, -1.341611e-02, -2.051174e-02, -1.107658e-02, 2.968712e-02, 1.014236e-01, 1.820007e-01, 2.357492e-01, 2.357492e-01, 1.820007e-01, 1.014236e-01, 2.968712e-02, -1.107658e-02, -2.051174e-02, -1.341611e-02, -4.420744e-03]

# N=28, Fpass=0.875, Fstop=5.375, 1:1
# h_updown = [-8.953714e-05, 4.107088e-05, 9.643965e-04, 2.889159e-03, 4.364589e-03, 1.985995e-03, -7.209208e-03, -2.106360e-02, -2.907346e-02, -1.534182e-02, 3.140924e-02, 1.065665e-01, 1.864240e-01, 2.381554e-01, 2.381554e-01, 1.864240e-01, 1.065665e-01, 3.140924e-02, -1.534182e-02, -2.907346e-02, -2.106360e-02, -7.209208e-03, 1.985995e-03, 4.364589e-03, 2.889159e-03, 9.643965e-04, 4.107088e-05, -8.953714e-05]

# N=28, Fpass=0.875, Fstop=5.375, 100:1  !!!!!!!!!!!!!!!!!!!!!!!
# h_updown = [-4.700191e-04, -1.152689e-03, -8.797074e-04, 1.909948e-03, 6.654939e-03, 8.677142e-03, 1.314495e-03, -1.689891e-02, -3.510881e-02, -3.150019e-02, 1.332802e-02, 9.850540e-02, 1.954160e-01, 2.602047e-01, 2.602047e-01, 1.954160e-01, 9.850540e-02, 1.332802e-02, -3.150019e-02, -3.510881e-02, -1.689891e-02, 1.314495e-03, 8.677142e-03, 6.654939e-03, 1.909948e-03, -8.797074e-04, -1.152689e-03, -4.700191e-04]

# N=64, Fpass=0.875, Fstop=5.375, 100:1
# h_updown = [-9.595459e-08, -6.332588e-07, -1.987938e-06, -3.270339e-06, -4.607331e-07, 1.257955e-05, 3.562707e-05, 4.819562e-05, 6.882171e-06, -1.218173e-04, -2.923842e-04, -3.311832e-04, -6.510245e-06, 7.357669e-04, 1.499989e-03, 1.441818e-03, -1.935606e-04, -3.144704e-03, -5.542184e-03, -4.563638e-03, 1.435333e-03, 1.046664e-02, 1.633891e-02, 1.165797e-02, -6.307904e-03, -3.049224e-02, -4.418346e-02, -2.849738e-02, 2.583458e-02, 1.094882e-01, 1.952017e-01, 2.494792e-01, 2.494792e-01, 1.952017e-01, 1.094882e-01, 2.583458e-02, -2.849738e-02, -4.418346e-02, -3.049224e-02, -6.307904e-03, 1.165797e-02, 1.633891e-02, 1.046664e-02, 1.435333e-03, -4.563638e-03, -5.542184e-03, -3.144704e-03, -1.935606e-04, 1.441818e-03, 1.499989e-03, 7.357669e-04, -6.510245e-06, -3.311832e-04, -2.923842e-04, -1.218173e-04, 6.882171e-06, 4.819562e-05, 3.562707e-05, 1.257955e-05, -4.607331e-07, -3.270339e-06, -1.987938e-06, -6.332588e-07, -9.595459e-08]


# N=4, Fpass=0.875, Fstop=5.375, 1:1
# h_updown = [1.933808e-01, 2.266248e-01, 2.266248e-01, 1.933808e-01]


# N=27, Fpass=0.875, Fstop=5.375, 1:1
# h_updown = [1.873048e-05, 5.247762e-04, 1.925389e-03, 3.694922e-03, 3.392723e-03, -2.400751e-03, -1.443979e-02, -2.647945e-02, -2.479811e-02, 5.177359e-03, 6.751977e-02, 1.477993e-01, 2.163813e-01, 2.434180e-01, 2.163813e-01, 1.477993e-01, 6.751977e-02, 5.177359e-03, -2.479811e-02, -2.647945e-02, -1.443979e-02, -2.400751e-03, 3.392723e-03, 3.694922e-03, 1.925389e-03, 5.247762e-04, 1.873048e-05, 0.0]


h_updown = np.array(h_updown)

filt_gen.build_upsampler_downsampler(h_updown)


if __name__ == "__main__":
    plt.style.use('dark_background')

    h_combined_updown = np.convolve(h_updown, h_updown)
    h_prac_with_updown = tools.downsample_filter(np.convolve(tools.upsample_filter(h_prac), h_combined_updown), offset=3)
    h_gold_with_updown = tools.downsample_filter(np.convolve(tools.upsample_filter(h_tx), h_combined_updown), offset=3)

    # tools.plot_freq_response(h_rx)
    # tools.plot_freq_response(np.convolve(tools.upsample_filter(h_prac), h_combined_updown))
    # plt.show()

    print("Gold TX and Gold RX: (Must meet 50dB MER, no OOB restrictions)")
    tools.print_stats(h_tx, h_rx)
    print("Practical TX and Gold RX: (Must meet 40dB MER, OOB numbers must be > 0)")
    tools.print_stats(h_prac, h_rx)
    print("Gold TX and Gold RX with updown sampler: Must meet 49dB MER")
    tools.print_stats(h_gold_with_updown, h_rx)
    print("Practical TX and Gold RX with updown sampler: Must meet 39dB MER")
    tools.print_stats(h_prac_with_updown, h_rx)
