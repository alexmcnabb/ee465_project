#!venv/bin/python
import sys
import os
import numpy as np
from math import pi, sin, cos, log, ceil
import scipy.signal as signal
from matplotlib import pyplot as plt

import tools

# plt.style.use('dark_background')

def get_adders(source_var, n_first_layer, adder_bit_length=36, clk_en=None):
    # Returns code block holding all adders, final result var name
    clk_enable_condition = "" if clk_en is None else f" if ({clk_en})"
    layers_of_adders = ceil(log(n_first_layer, 2))
    adder_parts = []
    prev_layer_size = n_first_layer  # Number of inputs into current layer
    for adder_layer in range(layers_of_adders):
        current_layer_size = ceil(prev_layer_size / 2)  # Number of outputs from the current layer
        num_adders = current_layer_size if prev_layer_size % 2 == 0 else current_layer_size - 1
        # print(f"Prev{prev_layer_size}, curr:{current_layer_size}, adders: {num_adders}")
        prev_layer_name = source_var if adder_layer == 0 else f"sum_level_{adder_layer-1}"
        current_layer_name = f"sum_level_{adder_layer}"

        adder_parts.append("")
        adder_parts.append(f"reg signed [{adder_bit_length-1}:0] {current_layer_name}[{current_layer_size-1}:0];")
        adder_parts.append(f"always @ (posedge clk) begin")
        adder_parts.append(f"    for (i = 0; i < {num_adders}; i = i + 1)")
        adder_parts.append(f"        if (reset)")
        adder_parts.append(f"            {current_layer_name}[i] <= {adder_bit_length}'b0;")
        adder_parts.append(f"        else{clk_enable_condition}")
        adder_parts.append(f"            {current_layer_name}[i] <= {prev_layer_name}[i] + {prev_layer_name}[i+{num_adders}];")
        if num_adders != current_layer_size:
            adder_parts.append(f"    if (reset)")
            adder_parts.append(f"        {current_layer_name}[{current_layer_size - 1}] <= {adder_bit_length}'b0;")
            adder_parts.append(f"    else{clk_enable_condition}")
            adder_parts.append(f"        {current_layer_name}[{current_layer_size - 1}] <= {prev_layer_name}[{prev_layer_size-1}];")
        adder_parts.append(f"end")
        prev_layer_size = current_layer_size

    final_var_name = f"sum_level_{layers_of_adders - 1}[0][{adder_bit_length-2}:{adder_bit_length-19}]"
    return '\n'.join(adder_parts), final_var_name


def build_filter_withmult(filename, h):
    h = np.array(h)
    assert all(h == h[::-1]), "Filter must be symmetric"
    code_parts = {}

    code_parts['module_name'] = "filter_tx_gold"



    code_parts['module_name'] = filename.split("/")[-1].split(".")[0]
    N = len(h)
    code_parts['N'] = str(N)
    N_2 = int((N + 1) / 2)
    code_parts['N_2'] = str(N_2)

    h = np.rint(np.array(h) * 2 ** 17)
    b_values = [f"{'-' if x < 0 else ''}18'sd{abs(int(x))}" for x in h[0:int((N + 1) / 2)]]
    code_parts['b_definition'] = "\n".join([f"assign b[{i}] = {b};" for i, b in enumerate(b_values)])

    adders, final_adder_var = get_adders("mult_out", N_2, clk_en="sam_clk_ena")
    code_parts['adder_definitions'] = adders
    code_parts['final_adder_output'] = final_adder_var

    code = open('template_filter.v').read()
    code = code.replace('{', '{{').replace('}', '}}').replace('[[[', '{').replace(']]]', '}')
    code = code.format(**code_parts)
    open(filename, 'w').write(code)

# def build_rx_filter(h):
#     h = np.array(h)
#     assert len(h) % 4 == 0, "Length of filter must be multiple of 4"
#     code_parts = {}
#     N = len(h)
#     code_parts['N'] = str(N)
#
#     h = np.rint(np.array(h) * 2 ** 17)
#     b_values = [f"{'-' if x < 0 else ''}18'sd{abs(int(x))}" for x in h[0:N]]
#     code_parts['b_definition'] = "\n".join([f"assign b[{i}] = {b};" for i, b in enumerate(b_values)])
#
#     adders, final_adder_var = get_adders("mult_out_shifted", N, clk_en="sym_clk_ena")
#     code_parts['adder_definitions'] = adders
#     code_parts['final_adder_output'] = final_adder_var
#
#     code = open('template_rx_filter.v').read()
#     code = code.replace('{', '{{').replace('}', '}}').replace('[[[', '{').replace(']]]', '}')
#     code = code.format(**code_parts)
#     open("../filter_rx_simplified.v", 'w').write(code)


def build_filter_multless(filename, h):
    h = np.array(h)
    assert all(h == h[::-1]), "Filter must be symmetric"
    code_parts = {}
    code_parts['module_name'] = filename.split("/")[-1].split(".")[0]
    N = len(h)
    code_parts['N'] = str(N)
    N_SEL = int((N + 3) / 4)
    code_parts['N_SEL'] = str(N_SEL)
    N_LARGER = N_SEL * 4
    code_parts['N_LARGER'] = str(N_LARGER)
    adder_bit_length = 36
    code_parts['lut_size'] = adder_bit_length

    # LUT Definitions
    all_b = [np.rint(np.array(h) * 2 ** 17 * (x * 2 ** (adder_bit_length-19))) for x in [-1, -1/3, 1, 1/3]]  # Grey mapping
    all_b_strings = [[f"{'-' if x < 0 else ''}{adder_bit_length}'sd{abs(int(x))}" for x in b[0:N]] for b in all_b]
    lut_parts = []
    for i in range(N_LARGER):
        if i >= N:
            lut_parts.append(f"    lut_outs[{i}] <= {adder_bit_length}'b0;")
        else:
            lut_parts.append(f"    case (x[{i}])")
            for j in range(4):
                lut_parts.append(f"        2'd{j}: lut_outs[{i}] <= {all_b_strings[j][i]};")
            lut_parts.append(f"    endcase")
    code_parts['lut_definitions'] = '\n'.join(lut_parts)

    adders, final_adder_var = get_adders("select_out", N_SEL, clk_en="sam_clk_ena")
    code_parts['adder_definitions'] = adders
    code_parts['final_adder_output'] = final_adder_var

    code = open('template_filter_multless.v').read()
    code = code.replace('{', '{{').replace('}', '}}').replace('[[[', '{').replace(']]]', '}')
    code = code.format(**code_parts)
    open(filename, 'w').write(code)


def build_upsampler_downsampler(h):
    h = np.array(h)
    assert len(h) % 4 == 0, "Length of filter must be multiple of 4"
    code_parts = {}
    N = len(h)
    code_parts['N'] = str(N)

    h = np.rint(np.array(h) * 2 ** 17)
    b_values = [f"{'-' if x < 0 else ''}18'sd{abs(int(x))}" for x in h[0:N]]
    code_parts['b_definition'] = "\n".join([f"assign b[{i}] = {b};" for i, b in enumerate(b_values)])

    adders, final_adder_var = get_adders("mult_out", N // 4)
    code_parts['adder_definitions'] = adders
    code_parts['final_adder_output'] = final_adder_var

    code = open('template_upsampler.v').read()
    code = code.replace('{', '{{').replace('}', '}}').replace('[[[', '{').replace(']]]', '}')
    code = code.format(**code_parts)
    open("../upsampler.v", 'w').write(code)

    # Alternate adder definitions for downsampler
    adders, final_adder_var = get_adders("mult_out_shifted", N, clk_en="sam_clk_ena")
    code_parts['adder_definitions'] = adders
    code_parts['final_adder_output'] = final_adder_var

    code = open('template_downsampler.v').read()
    code = code.replace('{', '{{').replace('}', '}}').replace('[[[', '{').replace(']]]', '}')
    code = code.format(**code_parts)
    open("../downsampler.v", 'w').write(code)


if __name__ == "__main__":
    build_filter_withmult("../example_filter_withmult.v", h=[0.1, 0.2, 0.3, 0.4, 0.5, 0.4, 0.3, 0.2, 0.1])
    build_filter_multless("../example_filter_multless.v", h=[0.1, 0.2, 0.3, 0.4, 0.5, 0.4, 0.3, 0.2, 0.1])
