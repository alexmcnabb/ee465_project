#!venv/bin/python
import sys
import os
import numpy as np
from math import pi, sin, cos
import scipy.signal as signal
from matplotlib import pyplot as plt
from time import time, process_time
from timeit import default_timer as timer
from math import log10
import scipy.fftpack as fftpack
from mpl_toolkits import mplot3d

from srrc_gen import get_srrc_samples
from tools import plot_samples, plot_freq_response
from filt_export import h_tx, h_prac, h_updown
import tools


plt.style.use('dark_background')


raw_signal = np.loadtxt("../functional_simulation/signal.txt")
raw_signal = raw_signal * signal.windows.hann(M=len(raw_signal))
raw_noise = np.loadtxt("../functional_simulation/noise.txt")
raw_noise = raw_noise * signal.windows.hann(M=len(raw_noise))



# tp1 = np.loadtxt("../functional_simulation/tp1_output.txt")
# tp1 = tp1 * signal.windows.hann(M=len(tp1))
tools.plot_freq_response(raw_signal)
# tp2 = np.loadtxt("../functional_simulation/tp2_output.txt")
# tp2 = tp2 * signal.windows.hann(M=len(tp2))
# tools.plot_freq_response(tp2)





# raw_input_samples = raw_input_samples / len(raw_input_samples) / 100

w, H = signal.freqz(raw_noise, worN=4096)
# plt.plot(w / 2 / pi, 20 * np.log10(abs(H)))
# plot_freq_response(h_prac)
#
# plt.show()
# exit()
# raw_input_samples = raw_input_samples * signal.windows.hann(M=len(raw_input_samples))

# raw_input_samples_upsampled = tools.upsample_filter(raw_input_samples)

# h_combined_updown = np.convolve(h_updown, h_updown)
# out = np.convolve(h_combined_updown, raw_input_samples_upsampled)
# out = raw_input_samples_upsampled
# plt.plot(out)
# plt.show()
# output_samples = tools.downsample_filter(out, offset=3)

# fft = np.fft.rfft(raw_input_samples_upsampled)
# freqs = np.fft.rfftfreq(len(raw_input_samples_upsampled))

# plt.plot(freqs, 20*np.log10(abs(fft) / 4))
# tools.plot_freq_response(out)

# measured_output_samples = np.loadtxt("../functional_simulation/measured_output_samples.txt")
# measured_output_samples = measured_output_samples * signal.windows.hann(M=len(measured_output_samples))

# print(len(output_samples), len(raw_input_samples))
# output_samples = output_samples[4:]  #16 for N=64, 6 for N=28
# output_samples = output_samples[:-(len(output_samples) - len(raw_input_samples))]
# print(len(output_samples), len(raw_input_samples))
#
# diff = (output_samples) - (raw_input_samples)
# plt.plot(diff)
# plt.plot(raw_input_samples)
# plt.plot(output_samples)
# plt.plot(measured_output_samples)
# plot_freq_response(raw_input_samples)
# plot_freq_response(output_samples)
# plot_freq_response(measured_output_samples)
#
# fft = np.fft.rfft(diff)
# freqs = np.fft.rfftfreq(len(diff))
# plt.plot(freqs, fft)

# print(raw_samples)
# print(sum(raw_samples) / len(raw_samples))
# print(sum(abs(raw_samples)) / len(raw_samples))

H = np.abs(H)
points = len(H)

H_signal = [H for w, H in zip(w, H) if 0.215 < w/2/pi < 0.285]
H_OB3 = [H for w, H in zip(w, H) if 0.005 < w/2/pi < 0.075]
sig, ob3 = 20*np.log10(np.sum(H_signal) / points), 20*np.log10(np.sum(H_OB3) / points)
print(sig, ob3, sig-ob3)


# plt.plot(w, 20*np.log10(abs(H) / 3e8))
# plot_freq_response(h_prac)
#
#
# # plt.legend()
plt.show()


'''
Numbers for Deliverable 3:
Gold standard - TX - 0 multipliers, N=72
Gold standard - RX - 36 multipliers, N=72
Practical     - TX - 0 multipliers, N=72, Kaiser window not used

Filter preformance:
(Negitive OOB numbers indicate fail, positive indicates passes spec)
Gold standard - Python simulation -      MER: 50.894, OB1: -3.438, OB2: -7.122, OB3: -7.304
              - Modelsim          -      MER: 50.3
Practical     - Python simulation -      MER: 40.764, OB1: 1.000, OB2: 1.000, OB3: 1.012
              - Modelsim (62.5MSamples) - MER: 40.2, 1.1390 0.8876 0.8694
              - Modelsim (12.5kSamples)-            -5.5584 -13.5818 -16.091 (fail)

'''