module mapper (
    input [1:0] data_in,
    input [17:0] reference_level, // Default value is 18'sd87381 (2/3)
    output reg signed [17:0] data_out
);

wire [16:0] reference_level_over_2;
assign reference_level_over_2 = reference_level[17:1];

// Reference level should be 2/3, outputting values -1, -1/3, 1/3, 1 - scaled by 2^17
always @ *
    case (data_in)
        3'b00: data_out <= -reference_level - reference_level_over_2; // -1.0
        3'b01: data_out <= -reference_level_over_2; // -0.33
        3'b10: data_out <= reference_level + reference_level_over_2;  // 1.0
        3'b11: data_out <= reference_level_over_2;  // 0.33
    endcase

endmodule
