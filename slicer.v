module slicer (
    // input clk,
    // input reset,
    // input sam_clk_ena,
    input signed [17:0] data_in,
    input [17:0] reference_level,
    output reg [1:0] data_out
);

// Reference levels should be -2/3, 0, 2/3s
always @ * begin
    data_out[1] = data_in > 18'sd0;
    data_out[0] = (data_in > -reference_level & data_out[1] == 1'b0) | (data_in < reference_level & data_out[1] == 1'b1);
end

endmodule

// 3'b00: data_out <= -reference_level - reference_level_over_2; // -1.0
// 3'b01: data_out <= -reference_level_over_2; // -0.33
// 3'b10: data_out <= reference_level + reference_level_over_2;  // 1.0
// 3'b11: data_out <= reference_level_over_2;  // 0.33