module receiver_single_channel (
    input clk,
    input reset,
    input sam_clk_ena,
    input sym_clk_ena,
    input signed [17:0] rf_in,
    input clear_accumulator,
    input signed [17:0] bb_bypass,
    output reg [1:0] data_out
);
parameter ACCUMULATION_BITS = 10; // Must match parameter in receiver.v, and the divisor in divresult

integer i;
wire [17:0] rx_filter_out, rx_filter_out_bbb;
reg [17:0] measured_reference_level;
wire [17:0] data_from_output_mapper;
reg signed [17:0] error;


filter_rx_gold rx_filt(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(rf_in), .y(rx_filter_out));
filter_rx_gold rx_filt_bbb(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(bb_bypass), .y(rx_filter_out_bbb));

reg signed [17:0] rx_filter_out_clocked;
always @ (posedge clk)
    if (reset)
        rx_filter_out_clocked <= 18'b0;
    else if (sym_clk_ena)
        rx_filter_out_clocked <= rx_filter_out;

reg signed [17:0] rx_filter_out_clocked_bbb;
always @ (posedge clk)
    if (reset)
        rx_filter_out_clocked_bbb <= 18'b0;
    else if (sym_clk_ena)
        rx_filter_out_clocked_bbb <= rx_filter_out_bbb;


// Slicer
slicer slicer_1(.data_in(rx_filter_out_clocked), .data_out(data_out), .reference_level(measured_reference_level));


// Mapper for error detection
mapper mapper_1(.data_in(data_out), .data_out(data_from_output_mapper), .reference_level(measured_reference_level));


// Error output
always @ *
    error <= rx_filter_out_clocked - data_from_output_mapper;


//Mapper out power calculation
reg signed [17:0] abs_rx_filter_out;
always @ *
  if (rx_filter_out_clocked[17] == 1'b1)
    abs_rx_filter_out = -rx_filter_out_clocked;
  else
    abs_rx_filter_out = rx_filter_out_clocked; // 1s17

reg [17+ACCUMULATION_BITS:0] decision_variable_accumulator;  // 1s17 << ACC_BITS == 11s17
always @ (posedge clk)
    if (reset | clear_accumulator)
        decision_variable_accumulator <= 36'b0;
    else if (sym_clk_ena)
        decision_variable_accumulator <= decision_variable_accumulator + abs_rx_filter_out;
always @ (posedge clear_accumulator, posedge reset)
    if (reset)
        // measured_reference_level <= 18'd11674; // Approximate value for system without up/downsampling
        measured_reference_level <= 18'd1460; // Approximate value for system with up/downsampling
    else
        measured_reference_level <= decision_variable_accumulator[17+ACCUMULATION_BITS:ACCUMULATION_BITS];  // Cut ACC_BITS off to divide by s^ACC_BITS leaving 1s17

reg [54:0] mapper_out_power;
always @ (posedge clk)
    // Need to multply by 1.25 for reasons
    mapper_out_power <= measured_reference_level * measured_reference_level * 18'sd81920; // 1s17 * 1s17 * 2s16 = 4s50
    // Constant multipication could be replaced by add and bitshift


// Accumulated Squared Error System
reg [35+ACCUMULATION_BITS:0] square_error_accumulator, accumulated_square_error, accumulated_square_error_scaled;
reg [35:0] error_product;
always @ *
    error_product <= error * error; // 2s34
always @ (posedge clk)
    if (reset | clear_accumulator)
        square_error_accumulator <= 54'b0;
    else if (sym_clk_ena)
        square_error_accumulator <= square_error_accumulator + error_product;
always @ (posedge clear_accumulator)
    accumulated_square_error <= square_error_accumulator; // 2s44
always @ *
    accumulated_square_error_scaled <= {2'b0, accumulated_square_error[36+ACCUMULATION_BITS-1:2]}; // 4s42


// Accumulated Error System
reg signed [17+ACCUMULATION_BITS:0] error_accumulator, accumulated_error;
always @ (posedge clk)
    if (reset | clear_accumulator)
        error_accumulator <= 1'b0;
    else if (sym_clk_ena)
        error_accumulator <= error_accumulator + error;
always @ (posedge clear_accumulator)
    accumulated_error <= error_accumulator; // 1s17 << ACC_BITS == 11s17


// MER Calculation
reg signed [100:0] divresult, divresult2;
always @ * begin
    divresult <= mapper_out_power / accumulated_square_error / 64; // 4s50 / 2s34+ACC_BITS ----  ACC=10->/64, ACC=12->/16, ACC=14->/4, ACC=16->/1, ACC=18->x4
    // Live div result -- (xs17 * xs17 * 2s16) / (xs34) * (2s16) == xs50/xs50 - May not be working right
    divresult2 <= (decision_variable_accumulator * decision_variable_accumulator * 18'sd81920) / (square_error_accumulator * 18'sd65536);
end


endmodule
