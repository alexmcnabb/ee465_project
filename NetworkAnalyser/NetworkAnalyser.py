import pyaudio
import numpy as np
from time import time, sleep
import threading
from matplotlib import pyplot as plt

fs = 44100


now = 0
f_out = 1
def output_callback(in_data, frame_count, time_info, status):
    global now
    t = np.linspace(now / fs, (now + frame_count) / fs, frame_count, False)
    now += frame_count
    note = np.sin(f_out * t * 2 * np.pi)
    audio = note * (2 ** 15 - 1) / np.max(np.abs(note))
    return audio.astype(np.int16), pyaudio.paContinue


input_buffer_lock = threading.Lock()
input_buffer = []
def input_callback(in_data, frame_count, time_info, status):
    with input_buffer_lock:
        input_buffer.append(in_data)
    return None, pyaudio.paContinue

p = pyaudio.PyAudio()
out_stream = p.open(format=pyaudio.paInt16, channels=1, rate=fs, output=True, stream_callback=output_callback)
out_stream.start_stream()

p = pyaudio.PyAudio()
in_stream = p.open(format=pyaudio.paInt16, channels=1, rate=fs, input=True, stream_callback=input_callback)
in_stream.start_stream()


def record_single_tone(f, length_seconds):
    global f_out
    global input_buffer
    f_out = f
    sleep(0.3)  # Record and output are not well synchronised.
    with input_buffer_lock:
        input_buffer = []

    # sleep(length_seconds)
    while len(input_buffer) < length_seconds:
        sleep(0.001)
    with input_buffer_lock:
        raw_bytes_arrays = input_buffer.copy()
    total_length = sum([len(b) for b in raw_bytes_arrays]) // 2
    output_array = np.zeros(total_length, dtype=np.int16)
    index = 0
    for segment in raw_bytes_arrays:
        new_data = np.frombuffer(segment, dtype=np.int16)
        output_array[index:index + len(segment) // 2] = new_data
        index += len(segment) // 2
    if max(output_array > 32000):
        print("Warning: Clipping detected")
    return output_array


def get_power_at_freq(f, length_seconds):
    input_data = record_single_tone(f, length_seconds)
    # Truncate to an integer number of periods
    real_length_seconds = length_seconds * 1024 / fs
    raw_num_periods = real_length_seconds * f
    integer_periods = int(raw_num_periods)
    if integer_periods == 0:
        print("Warning: Measured period too short for f =", f)
    else:
        input_data = input_data[0:int(integer_periods / f * fs)]
    # plt.plot(input_data)
    # plt.plot(np.square(input_data, dtype=np.int32))
    time_domain_power = np.sum(np.square(input_data, dtype=np.int32), dtype=np.int64) / len(input_data) / 2
    # print("TDP", time_domain_power)
    f_data = np.abs(np.fft.rfft(input_data)) / len(input_data)
    f_data = np.abs(f_data) ** 2
    freqs = np.fft.rfftfreq(len(input_data), d=1/fs)
    # plt.plot(freqs, f_data)
    print(raw_num_periods, len(input_data) / 1024)
    total_power = sum([f_data for f_data, freq in zip(f_data, freqs) if f - 5 < freq < f + 5])
    return total_power, time_domain_power






# record_single_tone(100, 3.5)  # System takes a while to start working
record_single_tone(100, 330)  # System takes a while to start working

# freqs = np.arange(400, 406, 5)
# for f2 in freqs:
#     freqs2 = [f2] * 30
#     powers = []
#     for freq in freqs2:
#         power, tdp = get_power_at_freq(freq, 2)
#         powers.append(power)
#     plt.plot(powers)

freqs = np.geomspace(30, 1000, 50)
powers = []
for freq in freqs:
    power, tdp = get_power_at_freq(freq, 2)
    powers.append(power)
plt.plot(powers)

# plt.plot(freqs, powers)
# powers = []
# for freq in freqs:
#     power, tdp = get_power_at_freq(freq, 5)
#     powers.append(power)
#
# plt.plot(freqs, powers)
# powers = []
# for freq in freqs:
#     power, tdp = get_power_at_freq(freq, 10)
#     powers.append(power)

# plt.plot(freqs, powers)
# plt.plot(record_single_tone(40, 1))

in_stream.close()
out_stream.close()
p.terminate()
plt.show()



# f = 40
# while True:
#     f = f + 10
#     print(len(record_single_tone(f, 0.5)))


# p = pyaudio.PyAudio()
#
# stream = p.open(format=8, channels=1, rate=fs, output=False)
# print(stream.read())


