module channel (
    input clk,
    input reset,
    input sam_clk_ena,
    input sym_clk_ena,
    input signed [17:0] rf_in,
    output reg signed [17:0] rf_out
);

wire noise_en;
assign noise_en = 1'b0;

reg delayed_reset;
always @ (posedge clk)
    delayed_reset <= reset;

wire signed [17:0] noise;
awgn_generator awgn_generator_1(.clk(clk), .clk_en(noise_en), .reset_n(~delayed_reset), .awgn_out(noise));

wire signed [17:0] gain;  // Should be a 9s9 number

// assign gain = 18'sd0; // Gain of 0
// assign gain = 18'sd512; // Gain of 1 (Lots of errors)
assign gain = 18'sd512 * 8; // Gain of 8 (Some errors)

reg signed [35:0] product;
always @ * begin
    // Trim from 10s26 to 1s17
    product <= rf_in * gain;
    rf_out <= noise + product[26:9];
end
endmodule
