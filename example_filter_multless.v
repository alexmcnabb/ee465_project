// This file is autogenerated from filt_gen.py

module example_filter_multless (
    input clk,
    input reset,
    input sam_clk_ena, // should run at 1/4 of clk frequency
    input sym_clk_ena, // should run at 1/16 of clk frequency
    input [1:0] x_in,
    output reg signed [17:0] y // 1s17
);

// Designed to update output on sam_clk_ena
// Input is a single impulse of value 1, 1/3, -1/3, -1, specified by x_in
// Additional input impulse is added to output every sym_clk_ena, and propagated through with sam_clk_ena

integer i;
parameter N = 9;
parameter N_LARGER = 12;  // N, rounded up to nearest multiple of four
parameter N_SEL = 3;  // Number of signal selectors (N/4)
parameter LUT_SIZE = 36;


// X Definition
reg [1:0] x[N-1:0];
always @ (posedge clk)
    if (reset)
        for (i = 0; i < N; i = i + 1)
            x[i] <= 2'b0;
    else if (sam_clk_ena) begin
        x[0] <= x_in;
        for (i = 1; i < N; i = i + 1)
            x[i] <= x[i-1];
    end


// Staggered sym_clk_ena definition
// This counts from 0 to 3, and indicates which of each four adjacent taps should output a signal for a given sample
reg [1:0] sym_clk_counter;
always @ (posedge clk)
    if (reset | sym_clk_ena)
        sym_clk_counter <= 2'b0;
    else if (sam_clk_ena)
        sym_clk_counter <= sym_clk_counter + 1'b1;


// LUT outputs
reg signed [LUT_SIZE-1:0] lut_outs[N_LARGER-1:0];

// Signal Selector
reg signed [LUT_SIZE-1:0] select_out[N_SEL-1:0];
always @ (posedge clk) begin
    if (reset)
        for (i = 0; i < N_SEL; i = i + 1)
            select_out[i] <= 1'b0;
    if (sam_clk_ena)
        for (i = 0; i < N_SEL; i = i + 1)
            case (sym_clk_counter)
                2'd0: select_out[i] <= lut_outs[i * 4 + 0];
                2'd1: select_out[i] <= lut_outs[i * 4 + 1];
                2'd2: select_out[i] <= lut_outs[i * 4 + 2];
                2'd3: select_out[i] <= lut_outs[i * 4 + 3];
                default: select_out[i] <= 1'b0;
            endcase
    end

// Adder Definitions

reg signed [35:0] sum_level_0[1:0];
always @ (posedge clk) begin
    for (i = 0; i < 1; i = i + 1)
        if (reset)
            sum_level_0[i] <= 36'b0;
        else if (sam_clk_ena)
            sum_level_0[i] <= select_out[i] + select_out[i+1];
    if (reset)
        sum_level_0[1] <= 36'b0;
    else if (sam_clk_ena)
        sum_level_0[1] <= select_out[2];
end

reg signed [35:0] sum_level_1[0:0];
always @ (posedge clk) begin
    for (i = 0; i < 1; i = i + 1)
        if (reset)
            sum_level_1[i] <= 36'b0;
        else if (sam_clk_ena)
            sum_level_1[i] <= sum_level_0[i] + sum_level_0[i+1];
end


// LUT Definitions
always @ * begin
    case (x[0])
        2'd0: lut_outs[0] <= -36'sd1717986918;
        2'd1: lut_outs[0] <= -36'sd572662306;
        2'd2: lut_outs[0] <= 36'sd1717986918;
        2'd3: lut_outs[0] <= 36'sd572662306;
    endcase
    case (x[1])
        2'd0: lut_outs[1] <= -36'sd3435973837;
        2'd1: lut_outs[1] <= -36'sd1145324612;
        2'd2: lut_outs[1] <= 36'sd3435973837;
        2'd3: lut_outs[1] <= 36'sd1145324612;
    endcase
    case (x[2])
        2'd0: lut_outs[2] <= -36'sd5153960755;
        2'd1: lut_outs[2] <= -36'sd1717986918;
        2'd2: lut_outs[2] <= 36'sd5153960755;
        2'd3: lut_outs[2] <= 36'sd1717986918;
    endcase
    case (x[3])
        2'd0: lut_outs[3] <= -36'sd6871947674;
        2'd1: lut_outs[3] <= -36'sd2290649225;
        2'd2: lut_outs[3] <= 36'sd6871947674;
        2'd3: lut_outs[3] <= 36'sd2290649225;
    endcase
    case (x[4])
        2'd0: lut_outs[4] <= -36'sd8589934592;
        2'd1: lut_outs[4] <= -36'sd2863311531;
        2'd2: lut_outs[4] <= 36'sd8589934592;
        2'd3: lut_outs[4] <= 36'sd2863311531;
    endcase
    case (x[5])
        2'd0: lut_outs[5] <= -36'sd6871947674;
        2'd1: lut_outs[5] <= -36'sd2290649225;
        2'd2: lut_outs[5] <= 36'sd6871947674;
        2'd3: lut_outs[5] <= 36'sd2290649225;
    endcase
    case (x[6])
        2'd0: lut_outs[6] <= -36'sd5153960755;
        2'd1: lut_outs[6] <= -36'sd1717986918;
        2'd2: lut_outs[6] <= 36'sd5153960755;
        2'd3: lut_outs[6] <= 36'sd1717986918;
    endcase
    case (x[7])
        2'd0: lut_outs[7] <= -36'sd3435973837;
        2'd1: lut_outs[7] <= -36'sd1145324612;
        2'd2: lut_outs[7] <= 36'sd3435973837;
        2'd3: lut_outs[7] <= 36'sd1145324612;
    endcase
    case (x[8])
        2'd0: lut_outs[8] <= -36'sd1717986918;
        2'd1: lut_outs[8] <= -36'sd572662306;
        2'd2: lut_outs[8] <= 36'sd1717986918;
        2'd3: lut_outs[8] <= 36'sd572662306;
    endcase
    lut_outs[9] <= 36'b0;
    lut_outs[10] <= 36'b0;
    lut_outs[11] <= 36'b0;
end


always @ *
    // Slice one bit off the top here to make output 1s17
    y <= sum_level_1[0][34:17];

endmodule
