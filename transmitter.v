module transmitter (
    input clk,
    input reset,
    input sam_clk_ena,
    input sym_clk_ena,
    input [1:0] data_in_i, data_in_q,
    output reg signed [17:0] bb_bypass_i, bb_bypass_q,
    output reg signed [17:0] rf_out
);
wire [17:0] bband_i, bband_q;
transmitter_single_channel transmitter_i(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .data_in(data_in_i), .bband_out(bband_i));
transmitter_single_channel transmitter_q(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .data_in(data_in_q), .bband_out(bband_q));

// Upsamplers for each baseband channel
reg signed [17:0] post_upsampler_i, post_upsampler_q;
upsampler upsampler_i(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(bband_i), .y(post_upsampler_i));
upsampler upsampler_q(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(bband_q), .y(post_upsampler_q));

// 'NCO' to multiply each signal by a sine/cos of f=1/4
// Equivelent to multiplying by a pulse train [0, 1, 0, -1, 0, 1, 0, -1...] for sin, and [1, 0, -1, 0, 1, 0, -1, 0... for cos
// The zeros line up nicely between the two NCO outputs, allowing us to combine the two signals in a single LUT
// Half the outputs here are zeros, so there could have been some multipliers saved in the upsampler by not computing those values
reg [1:0] nco_counter;
always @ (posedge clk)
    if (reset)
        nco_counter <= 2'd0;
    else
        nco_counter <= nco_counter + 1'b1;

reg signed [17:0] nco_out_i, nco_out_q;
always @ * begin
    // rf_out <= post_upsampler_q;
    case (nco_counter)
        2'd0: rf_out <= post_upsampler_q;
        2'd1: rf_out <= post_upsampler_i;
        2'd2: rf_out <= -post_upsampler_q;
        2'd3: rf_out <= -post_upsampler_i;
    endcase
end

always @ * begin
    bb_bypass_i <= bband_i;
    bb_bypass_q <= bband_q;
end

endmodule
