module transmitter_single_channel (
    input clk,
    input reset,
    input sam_clk_ena,
    input sym_clk_ena,
    input [1:0] data_in,
    output reg signed [17:0] bband_out
);
// Turns out this module wasn't really nessisary lol

// filter_tx_gold
filter_tx_practical
    tx_filt(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(data_in), .y(bband_out));

endmodule
