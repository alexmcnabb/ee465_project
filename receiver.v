module receiver (
    input clk,
    input reset,
    input sam_clk_ena,
    input sym_clk_ena,
    input signed [17:0] rf_in,
    input rollover,
    input signed [17:0] bb_bypass_i, bb_bypass_q,
    input  [1:0] lfsr_bypass_i, lfsr_bypass_q,
    output reg [1:0] data_out_i, data_out_q
);
// Initial value of nco_counter is important for syncronising TX and RX
// Seperate out I and Q channels and downconvert them with our fake downconverter operating at f=1/4
integer i;
reg [1:0] nco_counter;
always @ (posedge clk)
    if (reset)
        nco_counter <= 2'd1;
    else
        nco_counter <= nco_counter + 1'b1;
reg signed [17:0] nco_out_i, nco_out_q;
always @ * begin
    case (nco_counter)
        2'd0: nco_out_i <= 17'b0;
        2'd1: nco_out_i <= rf_in;
        2'd2: nco_out_i <= 17'b0;
        2'd3: nco_out_i <= -rf_in;
    endcase
    // nco_out_q <= rf_in;
    case (nco_counter)
        2'd0: nco_out_q <= rf_in;
        2'd1: nco_out_q <= 17'b0;
        2'd2: nco_out_q <= -rf_in;
        2'd3: nco_out_q <= 17'b0;
    endcase
end


// Accumulator clear counter
parameter ACCUMULATION_BITS = 10; // Changing this parameter requires changing the same ACCUMULATION_BITS and the divisor in divresult in receiver_single_channel.v
reg [ACCUMULATION_BITS-1:0] acc_counter;
always @ (posedge clk)
    if (reset)
        acc_counter <= 1'b0;
    else if (sym_clk_ena)
        acc_counter <= acc_counter + 1'b1;
reg clear_accumulator;
always @ *
    clear_accumulator <= acc_counter == 1'b0;


// Downsample each channel
reg signed [17:0] post_downsampler_i, post_downsampler_q;
downsampler downsampler_i(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(nco_out_i), .y(post_downsampler_i));
downsampler downsampler_q(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .x_in(nco_out_q), .y(post_downsampler_q));


// Decode the signal
receiver_single_channel receiver_i(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .rf_in(post_downsampler_i),
                                   .clear_accumulator(clear_accumulator), .data_out(data_out_i), .bb_bypass(bb_bypass_i));
receiver_single_channel receiver_q(.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .rf_in(post_downsampler_q),
                                   .clear_accumulator(clear_accumulator), .data_out(data_out_q), .bb_bypass(bb_bypass_q));


// Symbol correctness check
wire sym_correct_i, sym_correct_q;
assign sym_correct_i = lfsr_bypass_i == data_out_i;
assign sym_correct_q = lfsr_bypass_q == data_out_q;

// BER Measurement
reg [1:0] bitstream_counter;
always @ (posedge clk)
    if (reset)
        bitstream_counter <= 2'd3;
    else if (sam_clk_ena)
        bitstream_counter <= bitstream_counter + 1'b1;
reg reference_bitstream, output_bitstream, bit_error;
always @ * begin
    case (bitstream_counter)
        2'd0: reference_bitstream <= lfsr_bypass_i[0];
        2'd1: reference_bitstream <= lfsr_bypass_i[1];
        2'd2: reference_bitstream <= lfsr_bypass_q[0];
        2'd3: reference_bitstream <= lfsr_bypass_q[1];
    endcase
    case (bitstream_counter)
        2'd0: output_bitstream <= data_out_i[0];
        2'd1: output_bitstream <= data_out_i[1];
        2'd2: output_bitstream <= data_out_q[0];
        2'd3: output_bitstream <= data_out_q[1];
    endcase
end
always @ *
    bit_error <= reference_bitstream ^ output_bitstream;
reg [ACCUMULATION_BITS-1+2:0] ber_counter, ber_measurement;
initial begin #(1000); ber_counter <= 36'b0; end // Force a reset here after all the filters are initialized
always @ (posedge clk)
    if (clear_accumulator)
        ber_counter <= 0;
    else if (sam_clk_ena)
        ber_counter <= ber_counter + bit_error;
always @ (posedge clear_accumulator)
    ber_measurement <= ber_counter; // BER is calculated as ber_measurement / (2*(ACC_BITS + 2))

endmodule
