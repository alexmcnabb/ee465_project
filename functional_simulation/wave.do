onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /testbench/receiver_1/receiver_i/measured_reference_level
add wave -noupdate -radix decimal /testbench/receiver_1/receiver_q/measured_reference_level
add wave -noupdate /testbench/receiver_1/receiver_i/divresult
add wave -noupdate /testbench/receiver_1/receiver_q/divresult
add wave -noupdate /testbench/receiver_1/sym_correct_i
add wave -noupdate /testbench/receiver_1/sym_correct_q
add wave -noupdate /testbench/receiver_1/reference_bitstream
add wave -noupdate /testbench/receiver_1/output_bitstream
add wave -noupdate /testbench/receiver_1/bit_error
add wave -noupdate -radix unsigned /testbench/receiver_1/ber_counter
add wave -noupdate -radix unsigned /testbench/receiver_1/ber_measurement
add wave -noupdate -format Analog-Step -height 74 -max 60384.999999999993 -min -58293.0 -radix decimal /testbench/transmitter_1/bband_i
add wave -noupdate -format Analog-Step -height 74 -max 23954.0 -min -26343.0 -radix decimal /testbench/receiver_1/post_downsampler_i
add wave -noupdate -format Analog-Step -height 74 -max 9299.0000000000018 -min -9651.0 -radix decimal /testbench/receiver_1/receiver_i/rx_filter_out
add wave -noupdate -format Analog-Step -height 74 -max 7935.0000000000009 -min -7991.0 -radix decimal /testbench/receiver_1/receiver_i/rx_filter_out_clocked
add wave -noupdate -format Analog-Step -height 74 -max 64418.999999999993 -min -62130.0 -radix decimal /testbench/transmitter_1/bband_q
add wave -noupdate -format Analog-Step -height 74 -max 26560.999999999996 -min -27630.0 -radix decimal /testbench/receiver_1/post_downsampler_q
add wave -noupdate -format Analog-Step -height 74 -max 11945.000000000002 -min -10392.0 -radix decimal /testbench/receiver_1/receiver_q/rx_filter_out
add wave -noupdate -format Analog-Step -height 74 -max 8494.0 -min -8261.0 -radix decimal /testbench/receiver_1/receiver_q/rx_filter_out_clocked
add wave -noupdate /testbench/receiver_1/lfsr_bypass_i
add wave -noupdate /testbench/receiver_1/lfsr_bypass_q
add wave -noupdate /testbench/receiver_1/data_out_i
add wave -noupdate /testbench/receiver_1/data_out_q
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {45283 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 369
configure wave -valuecolwidth 117
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ns} {73500 ns}
