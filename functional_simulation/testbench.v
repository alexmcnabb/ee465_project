module testbench;

reg clk;
reg reset;

localparam PERIOD = 2;
localparam RESET_DELAY = 1;
localparam RESET_LENGTH = 1;
integer i;

// Clock generation
initial begin clk = 0; forever begin #(PERIOD/2); clk = ~clk; end end

// Reset generation
initial begin reset = 0; #(RESET_DELAY); reset = 1; #(RESET_LENGTH); reset = 0; end


// Clock enables
wire sam_clk_ena, sym_clk_ena;
clk_enable_gen clk_en1 ( .clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena));


// LFSR
wire [1:0] lfsr_out_i, lfsr_out_q;
wire rollover;
lfsr lfsr1 ( .clk(clk), .reset(reset), .sym_clk_ena(sym_clk_ena), .lfsr_out_i(lfsr_out_i), .lfsr_out_q(lfsr_out_q), .rollover(rollover));


// Transmitter
wire signed [17:0] bb_bypass_i, bb_bypass_q;
wire signed [17:0] rf_tx_out, rf_channel_out;
transmitter transmitter_1 (.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), 
                           .data_in_i(lfsr_out_i), .data_in_q(lfsr_out_q), .rf_out(rf_tx_out), .bb_bypass_i(bb_bypass_i), .bb_bypass_q(bb_bypass_q));


channel channel_1 (.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena),  .rf_in(rf_tx_out), .rf_out(rf_channel_out));


// Delay numbers
// Steps of 1 change the system clock offset, affecting the first downsampler and the updown conversion
// Steps of four change the sample clock offset, affecting the downsampler after the receive filter
// Steps of 16 have no effect
// The inital value of nco_counter in receiver.v should be decremented if this is incremented and vice versa to keep the up and down conversion aligned
parameter SMALL_DELAY = 11;
reg [17:0] small_delay_rf[SMALL_DELAY-1:0];
always @ (posedge clk)
    if (reset)
        for (i = 0; i < SMALL_DELAY; i = i + 1)
            small_delay_rf[i] <= 0;
    else begin
        small_delay_rf[0] <= rf_channel_out;
        for (i = 1; i < SMALL_DELAY; i = i + 1)
            small_delay_rf[i] <= small_delay_rf[i-1];
    end

parameter BB_BYPASS_DELAY = 14; // Each step equivelent to 4 SMALL_DELAY
reg [17:0] bb_bypass_i_delay[BB_BYPASS_DELAY-1:0], bb_bypass_q_delay[BB_BYPASS_DELAY-1:0];
always @ (posedge clk)
    if (reset)
        for (i = 0; i < BB_BYPASS_DELAY; i = i + 1) begin
            bb_bypass_i_delay[i] <= 0;
            bb_bypass_q_delay[i] <= 0;
        end
    else if (sam_clk_ena == 1'b1) begin
        bb_bypass_i_delay[0] <= bb_bypass_i;
        bb_bypass_q_delay[0] <= bb_bypass_q;
        for (i = 1; i < BB_BYPASS_DELAY; i = i + 1) begin
            bb_bypass_i_delay[i] <= bb_bypass_i_delay[i-1];
            bb_bypass_q_delay[i] <= bb_bypass_q_delay[i-1];
        end
    end

// LFSR Bypass Delay for output data comparison
parameter SYSTEM_DELAY = 26; // Total system delay in symbols
reg [1:0] delay_i[SYSTEM_DELAY-1:0], delay_q[SYSTEM_DELAY-1:0];
always @ (posedge clk)
    if (sym_clk_ena) begin
        delay_i[0] <= lfsr_out_i;
        delay_q[0] <= lfsr_out_q;
        for (i = 1; i < SYSTEM_DELAY; i = i + 1) begin
            delay_i[i] <= delay_i[i-1];
            delay_q[i] <= delay_q[i-1];
        end
    end

// Receiver
wire signed [1:0] data_out_i, data_out_q;
receiver receiver_1 (.clk(clk), .reset(reset), .sam_clk_ena(sam_clk_ena), .sym_clk_ena(sym_clk_ena), .rf_in(small_delay_rf[SMALL_DELAY-1]),
                     .data_out_i(data_out_i), .data_out_q(data_out_q), .rollover(rollover), .bb_bypass_i(bb_bypass_i_delay[BB_BYPASS_DELAY-1]), 
                     .bb_bypass_q(bb_bypass_q_delay[BB_BYPASS_DELAY-1]), .lfsr_bypass_i(delay_i[SYSTEM_DELAY-1]), .lfsr_bypass_q(delay_q[SYSTEM_DELAY-1]));


// integer f, g;
// initial begin
//   f = $fopen("tp1_output.txt","w");
//   g = $fopen("tp2_output.txt","w");
// end

// always @ (posedge clk) begin
//     if (!$isunknown(rf_tx_out))
//         $fwrite(f,"%d\n", $signed(rf_tx_out));
//     if (!$isunknown(rf_channel_out))
//         $fwrite(g,"%d\n", $signed(rf_channel_out));
// end

endmodule

/*

MER values, Gold filter, no noise, 18 bit measurement - 50.86dB
    Reference levels of 1460 and 1458 were seen, producing MERs of 50.861dB and 50.013dB. Using larger value.

MER values, Prac filter, no noise, 16 bit measurement - 40.63dB


*/